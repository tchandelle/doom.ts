export interface Point {
  x: number,
  y: number,
}

export const lNodes: Point[][] = [
  // Episode 0 World Map
  [
  // location of level 0 (CJ)
    { x: 185, y: 164 },
    // location of level 1 (CJ)
    { x: 148, y: 143 },
    // location of level 2 (CJ)
    { x: 69, y: 122 },
    // location of level 3 (CJ)
    { x: 209, y: 102 },
    // location of level 4 (CJ)
    { x: 116, y: 89 },
    // location of level 5 (CJ)
    { x: 166, y: 55 },
    // location of level 6 (CJ)
    { x: 71, y: 56 },
    // location of level 7 (CJ)
    { x: 135, y: 29 },
    // location of level 8 (CJ)
    { x: 71, y: 24 },
  ],

  // Episode 1 World Map should go here
  [
  // location of level 0 (CJ)
    { x: 254, y: 25 },
    // location of level 1 (CJ)
    { x: 97, y: 50 },
    // location of level 2 (CJ)
    { x: 188, y: 64 },
    // location of level 3 (CJ)
    { x: 128, y: 78 },
    // location of level 4 (CJ)
    { x: 214, y: 92 },
    // location of level 5 (CJ)
    { x: 133, y: 130 },
    // location of level 6 (CJ)
    { x: 208, y: 136 },
    // location of level 7 (CJ)
    { x: 148, y: 140 },
    // location of level 8 (CJ)
    { x: 235, y: 158 },
  ],

  // Episode 2 World Map should go here
  [
  // location of level 0 (CJ)
    { x: 156, y: 168 },
    // location of level 1 (CJ)
    { x: 48, y: 154 },
    // location of level 2 (CJ)
    { x: 174, y: 95 },
    // location of level 3 (CJ)
    { x: 265, y: 75 },
    // location of level 4 (CJ)
    { x: 130, y: 48 },
    // location of level 5 (CJ)
    { x: 279, y: 23 },
    // location of level 6 (CJ)
    { x: 198, y: 48 },
    // location of level 7 (CJ)
    { x: 140, y: 25 },
    // location of level 8 (CJ)
    { x: 281, y: 136 },
  ],

]
