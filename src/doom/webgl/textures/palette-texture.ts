import { DataTexture, RGBAFormat } from 'three'
import { ColorMap } from '../../interfaces/colormap'
import { Palette } from '../../interfaces/palette'

export class PaletteTexture extends DataTexture {
  private data: Uint8Array
  private _palette: Palette
  set palette(p: Palette) {
    if (p !== this._palette) {
      this._palette = p
      this.update()
    }
  }
  get palette() {
    return this._palette
  }

  private _colorMap: ColorMap
  set colorMap(m: ColorMap) {
    if (m !== this._colorMap) {
      this._colorMap = m
      this.update()
    }
  }
  get colorMap() {
    return this._colorMap
  }

  constructor(palette: Palette, colorMap: ColorMap) {
    const data = new Uint8Array(256 * 4)
    super(data, 256, 1, RGBAFormat)

    this.data = data
    this._palette = palette
    this._colorMap = colorMap
    this.colorSpace = 'srgb'
    this.update()
  }

  update(): void {
    const gamma = 0
    const palette = this._palette
    let mapped: number

    for (let i = 0; i < 256; ++i) {
      mapped = this._colorMap[i]
      this.data.set(
        palette.get(mapped, gamma),
        i * 4,
      )
    }

    this.needsUpdate = true
  }
}
