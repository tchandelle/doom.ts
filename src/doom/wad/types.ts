//
// WADFILE I/O related stuff.
//
export interface LumpInfo {
  name: string
  buffer: ArrayBuffer
  size: number
}
