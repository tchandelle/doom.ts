import { ANG90, ANGLE_TO_FINE_SHIFT, FINE_ANGLES, fineSine } from '../misc/table'
import { LIGHT_LEVELS, LIGHT_SEG_SHIFT, LIGHT_Z_SHIFT, MAX_LIGHT_Z, Rendering } from './rendering'
import { div, mul } from '../misc/fixed'
import { ANGLE_TO_SKY_SHIFT } from '../level/sky'
import { ColorMap } from '../interfaces/colormap'
import { Data } from './data'
import { Draw } from './draw'
import { Level } from '../level/level'
import { RANGE_CHECK } from '../global/doomdef'
import { Things } from './things'
import { VisPlane } from './plane/vis-plane'

// Here comes the obnoxious "visplane".
export const MAX_VISPLANES = 128
export class Plane {
  //
  // opening
  //

  // Here comes the obnoxious "visplane".
  private visPlanes: VisPlane[]
  private lastVisPlanePtr = -1
  floorPlane: VisPlane | null = null
  ceilingPlane: VisPlane | null = null

  // Add SCREENWIDTH for underflow and overflow
  openings: Int16Array
  lastOpeningPtr = 0

  //
  // Clip values are the solid pixel bounding the range.
  //  floorclip starts out SCREENHEIGHT
  //  ceilingclip starts out -1
  //
  floorClip: Int16Array
  ceilingClip: Int16Array

  //
  // spanstart holds the start of a plane span
  // initialized to 0 at start
  //
  private spanStart: number[]

  //
  // texture mapping
  //
  protected planeZLight = new Array<ColorMap>()
  private planeHeight = 0

  ySlope: number[]
  distScale: number[]
  private baseXScale = 0
  private baseYScale = 0

  private cachedHeight: number[]
  private cachedDistance: number[]
  private cachedXStep: number[]
  private cachedYStep: number[]

  private get data(): Data {
    return this.rendering.data
  }
  private get draw(): Draw {
    return this.rendering.draw
  }
  protected get level(): Level {
    return this.rendering.level
  }
  private get things(): Things {
    return this.rendering.things
  }

  constructor(protected rendering: Rendering,
    width: number, height: number,
  ) {
    const maxOpenings = width * 64

    this.visPlanes = Array.from({ length: MAX_VISPLANES }, () => new VisPlane(width))

    this.openings = new Int16Array(width * 2 + maxOpenings)
    this.floorClip = new Int16Array(width).fill(0)
    this.ceilingClip = new Int16Array(width).fill(0)

    this.spanStart = new Array<number>(height).fill(0)
    this.ySlope = new Array<number>(height).fill(0)
    this.distScale = new Array<number>(width).fill(0)
    this.cachedHeight = new Array<number>(height).fill(0)
    this.cachedDistance = new Array<number>(height).fill(0)
    this.cachedXStep = new Array<number>(height).fill(0)
    this.cachedYStep = new Array<number>(height).fill(0)
  }

  //
  // R_MapPlane
  //
  // Uses global vars:
  //  planeheight
  //  ds_source
  //  basexscale
  //  baseyscale
  //  viewx
  //  viewy
  //
  // BASIC PRIMITIVE
  //
  mapPlane(y: number, x1: number, x2: number): void {

    if (RANGE_CHECK) {
      if (x2 < x1 ||
          x1 < 0 ||
          x2 >= this.draw.viewWidth ||
          y >>> 0 > this.draw.viewHeight
      ) {
        throw `R_MapPlane: ${x1}, ${x2} at ${y}`
      }
    }

    let distance: number
    if (this.planeHeight !== this.cachedHeight[y]) {
      this.cachedHeight[y] = this.planeHeight
      distance = this.cachedDistance[y] =
        mul(this.planeHeight, this.ySlope[y])
      this.draw.dsXStep = this.cachedXStep[y] =
        mul(distance, this.baseXScale)
      this.draw.dsYStep = this.cachedYStep[y] =
        mul(distance, this.baseYScale)
    } else {
      distance = this.cachedDistance[y]
      this.draw.dsXStep = this.cachedXStep[y]
      this.draw.dsYStep = this.cachedYStep[y]
    }

    const length = mul(distance, this.distScale[x1])
    const angle = this.rendering.viewAngle + this.rendering.xToViewAngle[x1] >>> ANGLE_TO_FINE_SHIFT
    this.draw.dsXFrac = this.rendering.viewX + mul(fineSine[FINE_ANGLES / 4 + angle], length)
    this.draw.dsYFrac = -this.rendering.viewY - mul(fineSine[angle], length)

    if (this.rendering.fixedColorMap) {
      this.draw.dsColorMap = this.rendering.fixedColorMap
    } else {
      let index = distance >>> LIGHT_Z_SHIFT

      if (index >= MAX_LIGHT_Z) {
        index = MAX_LIGHT_Z - 1
      }
      this.draw.dsColorMap = this.planeZLight[index]
    }

    this.draw.dsY = y
    this.draw.dsX1 = x1
    this.draw.dsX2 = x2

    // high or low detail
    if (this.rendering.spanFunc === null) {
      throw 'this.rendering.spanFunc = null'
    }
    this.rendering.spanFunc.apply(this.draw)
  }


  //
  // R_ClearPlanes
  // At begining of frame.
  //
  clearPlanes(): void {
    // opening / clipping determination
    for (let i = 0; i < this.draw.viewWidth; ++i) {
      this.floorClip[i] = this.draw.viewHeight
      this.ceilingClip[i] = -1
    }

    this.lastVisPlanePtr = 0
    this.lastOpeningPtr = 0

    // texture calculation
    this.cachedHeight.fill(0)

    // left to right mapping
    const angle = this.rendering.viewAngle - ANG90
        >>> ANGLE_TO_FINE_SHIFT

    // scale will be unit scale at SCREENWIDTH/2 distance
    this.baseXScale = div(fineSine[FINE_ANGLES / 4 + angle], this.rendering.centerXFrac)
    this.baseYScale = -div(fineSine[angle], this.rendering.centerXFrac)
  }

  //
  // R_FindPlane
  //
  findPlane(height: number, picNum: number, lightLevel: number): VisPlane {
    if (picNum === this.level.sky.flatNum) {
      // all skys map together
      height = 0
      lightLevel = 0
    }

    let checkPtr: number
    let check: VisPlane
    for (checkPtr = 0, check = this.visPlanes[checkPtr];
      checkPtr < this.lastVisPlanePtr;
      checkPtr++, check = this.visPlanes[checkPtr]
    ) {
      if (height === check.height &&
        picNum === check.picNum &&
        lightLevel === check.lightLevel
      ) {
        break
      }
    }

    if (checkPtr < this.lastVisPlanePtr) {
      return check
    }

    if (this.lastVisPlanePtr === MAX_VISPLANES) {
      throw 'R_FindPlane: no more visplanes'
    }

    this.lastVisPlanePtr++

    check.height = height
    check.picNum = picNum
    check.lightLevel = lightLevel
    check.minX = this.rendering.video.width
    check.maxX = -1

    check.top.fill(0xffff)

    return check
  }

  //
  // R_CheckPlane
  //
  checkPlane(pl: VisPlane, start: number, stop: number): VisPlane {
    let intrl: number
    let intrh: number
    let unionl: number
    let unionh: number
    if (start < pl.minX) {
      intrl = pl.minX
      unionl = start
    } else {
      unionl = pl.minX
      intrl = start
    }

    if (stop > pl.maxX) {
      intrh = pl.maxX
      unionh = stop
    } else {
      unionh = pl.maxX
      intrh = stop
    }

    let x: number
    for (x = intrl; x <= intrh; ++x) {
      if (pl.top[x] !== 0xffff) {
        break
      }
    }

    if (x > intrh) {
      pl.minX = unionl
      pl.maxX = unionh

      // use the same one
      return pl
    }

    // make a new visplane
    const lastVisplane = this.visPlanes[this.lastVisPlanePtr]
    lastVisplane.height = pl.height
    lastVisplane.picNum = pl.picNum
    lastVisplane.lightLevel = pl.lightLevel

    pl = this.visPlanes[this.lastVisPlanePtr++]
    pl.minX = start
    pl.maxX = stop

    pl.top.fill(0xffff)

    return pl
  }

  //
  // R_MakeSpans
  //
  makeSpans(x: number, t1: number, b1: number, t2: number, b2: number): void {
    // console.log(`ms ${x} ${t1} ${b1} ${t2} ${b2}`)
    while (t1 < t2 && t1 <= b1) {
      this.mapPlane(t1, this.spanStart[t1], x - 1)
      ++t1
    }
    while (b1 > b2 && b1 >= t1) {
      this.mapPlane(b1, this.spanStart[b1], x - 1)
      --b1
    }

    while (t2 < t1 && t2 <= b2) {
      this.spanStart[t2] = x
      ++t2
    }
    while (b2 > b1 && b2 >= t2) {
      this.spanStart[b2] = x
      --b2
    }
  }

  //
  // R_DrawPlanes
  // At the end of each frame.
  //
  drawPlanes(): void {

    if (RANGE_CHECK) {
      if (this.lastVisPlanePtr > MAX_VISPLANES) {
        throw `R_DrawPlanes: visplane overflow (${this.lastVisPlanePtr})`
      }
      // if (this.lastOpeningPtr > MAX_OPENINGS) {
      //   throw `R_DrawPlanes: opening overflow (${this.lastOpeningPtr})`
      // }

    }

    let pl: VisPlane
    let x: number
    let stop: number
    let angle: number
    for (let plPtr = 0; plPtr < this.lastVisPlanePtr; ++plPtr) {
      pl = this.visPlanes[plPtr]
      if (pl.minX > pl.maxX) {
        continue
      }

      // sky flat
      if (pl.picNum === this.level.sky.flatNum) {
        this.draw.dcIScale = this.things.pSpriteIScale >> this.rendering.detailShift

        // Sky is allways drawn full bright,
        //  i.e. colormaps[0] is used.
        // Because of this hack, sky is not affected
        //  by INVUL inverse mapping.
        this.draw.dcColorMap = this.data.colorMaps.c[0]
        this.draw.dcTextureMid = this.level.sky.textureMid
        for (x = pl.minX; x <= pl.maxX; ++x) {
          this.draw.dcYl = pl.top[x]
          this.draw.dcYh = pl.bottom[x]

          if (this.draw.dcYl <= this.draw.dcYh) {
            angle = this.rendering.viewAngle + this.rendering.xToViewAngle[x] >> ANGLE_TO_SKY_SHIFT
            this.draw.dcX = x
            this.draw.dcSource =
              this.data.textures.getColumn(this.level.sky.texture, angle).posts[0]

            if (this.rendering.colFunc === null) {
              throw 'this.rendering.colFunc = null'
            }
            this.rendering.colFunc.apply(this.draw)
          }
        }
        continue
      }

      // regular flat
      this.draw.dsSource = this.data.flats.get(pl.picNum)

      this.planeHeight = Math.abs(pl.height - this.rendering.viewZ)

      this.calculateLights(pl.lightLevel)

      pl.top[pl.maxX + 1] = 0xffff
      pl.top[pl.minX - 1] = 0xffff

      stop = pl.maxX + 1

      for (x = pl.minX; x <= stop; ++x) {
        this.makeSpans(
          x,
          pl.top[x - 1],
          pl.bottom[x - 1],
          pl.top[x],
          pl.bottom[x],
        )
      }
    }
  }

  calculateLights(lightLevel: number): ColorMap[] {
    let light = (lightLevel >> LIGHT_SEG_SHIFT) + this.rendering.extraLight

    if (light >= LIGHT_LEVELS) {
      light = LIGHT_LEVELS - 1
    }

    if (light < 0) {
      light = 0
    }

    return this.planeZLight = this.rendering.zLight[light]
  }
}
