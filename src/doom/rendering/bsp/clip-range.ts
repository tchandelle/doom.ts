//
// ClipWallSegment
// Clips the given range of columns
// and includes it in the new clip list.
//
export interface ClipRange {
  first: number;
  last: number;
}
