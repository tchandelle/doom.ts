//
// Move clipping aid for LineDefs.
//
export const enum SlopeType {
  Horizontal,
  Vertical,
  Positive,
  Negative
}
