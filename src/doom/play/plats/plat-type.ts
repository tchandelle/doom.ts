export const enum PlatType {
  PerpetualRaise,
  DownWaitUpStay,
  RaiseAndChange,
  RaiseToNearestAndChange,
  BlazeDWUS,
}
