export const enum CeilingType {
  LowerToFloor,
  RaiseToHighest,
  LowerAndCrush,
  CrushAndRaise,
  FastCrushAndRaise,
  SilentCrushAndRaise,
}
