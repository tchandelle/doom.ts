import { Thinker, noopFunc } from '../doom/think'
import { Game } from '../game/game'
import { MAX_PLAYERS } from '../global/doomdef'
import { MObjHandler } from './mobj-handler'
import { Play } from './setup'
import { Special } from './special'
import { User } from './user'

//
// THINKERS
// All thinkers should be allocated by Z_Malloc
// so they can be operated on uniformly.
// The actual structures will vary in size,
// but the first element must be thinker_t.
//

export class Tick {
  levelTime = -1

  // Both the head and tail of the thinker list.
  thinkerCap = new Thinker(null, this)

  private get game(): Game {
    return this.play.game
  }
  private get mObjHandler(): MObjHandler {
    return this.play.mObjHandler
  }
  private get special(): Special {
    return this.play.special
  }
  private get user(): User {
    return this.play.user
  }
  constructor(private play: Play) { }

  //
  // P_InitThinkers
  //
  initThinkers(): void {
    this.thinkerCap.prev = this.thinkerCap.next = this.thinkerCap
  }

  //
  // P_AddThinker
  // Adds a new thinker at the end of the list.
  //
  addThinker<H, A extends unknown[]>(thinker: Thinker<H, A>): void {
    if (this.thinkerCap.prev === null) {
      throw 'this.thinkerCap.prev = null'
    }
    this.thinkerCap.prev.next = thinker
    thinker.next = this.thinkerCap
    thinker.prev = this.thinkerCap.prev
    this.thinkerCap.prev = thinker
  }

  //
  // P_RemoveThinker
  // Deallocation is lazy -- it will not actually be freed
  // until its thinking turn comes up.
  //
  removeThinker<H, A extends unknown[]>(thinker: Thinker<H, A>): void {
    thinker.func = noopFunc
  }

  //
  // P_RunThinkers
  //
  private runThinkers(): void {
    let currentThinker = this.thinkerCap.next

    while (currentThinker !== null &&
      currentThinker !== this.thinkerCap
    ) {
      if (currentThinker.func === noopFunc) {
        if (currentThinker.next === null) {
          throw 'currentThinker.next = null'
        }
        if (currentThinker.prev === null) {
          throw 'currentThinker.next = null'
        }
        // time to remove it
        currentThinker.next.prev = currentThinker.prev
        currentThinker.prev.next = currentThinker.next
      } else {
        if (currentThinker.func !== null) {
          currentThinker.func.call(currentThinker.handler, currentThinker)
        }
      }
      currentThinker = currentThinker.next
    }
  }

  //
  // P_Ticker
  //
  ticker(): void {
    // run the tic
    if (this.game.paused) {
      return
    }

    for (let i = 0; i < MAX_PLAYERS; ++i) {
      if (this.game.playerInGame[i]) {
        this.user.playerThink(this.game.players[i])
      }
    }

    this.runThinkers()
    this.special.updateSpecials()
    this.mObjHandler.respawnSpecials()

    // for par times
    this.levelTime++
  }
}
