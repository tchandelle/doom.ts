export const enum DoorType {
  Normal,
  Close30ThenOpen,
  Close,
  Open,
  RaiseIn5Mins,
  BlazeRaise,
  BlazeOpen,
  BlazeClose,
}
