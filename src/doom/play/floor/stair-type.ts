export const enum StairType {
  // slowly build by 8
  Build8,
  // quickly build by 16
  Turbo16,
}
